# go-lp-map

moved to [[https://bitbucket.org/mindaugas_z/go-silent-snake/src/master/libs/]]

map value set

```go
func main() {
	m := lpmap.New()

	if m.Set("some-value") {
		println("do job")
	}

	if !m.Set("some-value") {
		println("dont do job")
    }
    
    if m.Exists("some-value") {
        // do something
        m.Delete("some-value")
	}
	
	// m.Reset()
}
```